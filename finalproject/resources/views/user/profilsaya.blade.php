@extends('master')

@section('content')

<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- LEFT COLUMN -->
                    <div class="profile-left">
                        <!-- PROFILE HEADER -->
                        <div class="profile-header">
                            <div class="overlay"></div>
                            <div class="profile-main">
                                <img src="{{ $detailprofile->getAvatar()}}" class="img-circle" width="50%" alt="Avatar">
                                <h3 class="name">{{ $detailprofile->name }}</h3>
                                <span class="online-status status-available">Available</span>
                            </div>
                        </div>
                        <!-- END PROFILE HEADER -->
                        <!-- PROFILE DETAIL -->
                        <div class="text-center">
                            <a href="/profile/{{ $detailprofile->id }}/edit" class="btn btn-warning">Edit Profile</a>
                        </div>
                        <!-- END PROFILE DETAIL -->
                    </div>
                    <!-- END LEFT COLUMN -->
                    <!-- RIGHT COLUMN -->
                    <div class="profile-right">
                        <!-- TABBED CONTENT -->
                        <div class="custom-tabs-line tabs-line-bottom left-aligned">
                            <ul class="nav" role="tablist">
                                <li class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab">Data Diri</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-bottom-left1">
                                <div class="profile-detail">
                                    <div class="profile-info">
                                        <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">Umur</th>
                                            <th scope="col">Alamat</th>
                                            <th scope="col">Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <td>{{ $detailprofile->umur }}</td>
                                            <td>{{ $detailprofile->alamat }}</td>
                                            <td>{{ $detailprofile->email }}</td>
                                            </tr>
                                        </tbody>
                                        </table>
                                    </div>
                                    <div class="profile-info">
                                        <h3>Biodata</h3>
                                        <p>{{ $detailprofile->biodata }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END TABBED CONTENT -->
                    </div>
                    <!-- END RIGHT COLUMN -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>

@endsection