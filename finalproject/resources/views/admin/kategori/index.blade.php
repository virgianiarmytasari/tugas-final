@extends('master')

@section('content')

<div class="main">
  <div class="main-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading">
              <h1 class="panel-title"><b>Table Kategori</b></h1>
              
            </div>
            <div class="panel-body">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Hastag</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data_kategori as $kategori)
                  <tr>
                    <td>{{ $kategori->tag_name }}</td>
                    <td>
                      <form action="kategori/{{$kategori->id}}/hapus" method="POST" class="d-inline">
                          @method('delete')
                          @csrf
                      <button  class="submit btn badge-danger">Hapus</button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection