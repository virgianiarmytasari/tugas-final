<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\pertanyaan;
use App\Models\kategori;
use App\Models\User;
use App\Models\komentar;

class PertanyaanController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari'))
        {
            $pertanyaan = pertanyaan::Where('judul', 'LIKE', '%'.$request->cari.'%')
                            ->get();
        }else{
            $pertanyaan = pertanyaan::orderBy('created_at','desc')->get();
        };
        return view('user.forum.index',compact(['pertanyaan']));
    }

    public function add()
    {
        return view('user.forum.add');
    }

    public function create(Request $request)
    {
        $kategori_arr = explode(',',$request["kategori"]);

        // looping array
        // array kosong
        $kategori_id = [];
        foreach($kategori_arr as $tag_name){
            // mencari tagname
            $tag = kategori::firstOrCreate(['tag_name' => $tag_name]);
            $kategori_id[] = $tag->id;
        }

        $pertanyaan = new pertanyaan;
        $pertanyaan->judul = $request->judul;
        $pertanyaan->isi = $request->isi;
        $pertanyaan->users_id = auth()->user()->id;
        $pertanyaan->save();
        
        // menyimpan id
        $pertanyaan->kategori()->sync($kategori_id);
        // menambahkan ke pertayaan_id  baru di pertanyaan_kategori
        $user = Auth::user();
        $user->pertanyaan()->save($pertanyaan);
        
        
        // Pertanyaan::create(['judul' => $request->judul, 'isi' => $request->isi, 'user_id' => $request->profile]);
        return redirect('/forum')->with('sukses','data anda berhasil di tambahkan');
    }

    public function view($id){
        $pertanyaan = pertanyaan::find($id);
        $user = User::where('id', Auth::user()->id)->get();
        return view('user.forum.view', compact(['pertanyaan','user']));
    }

    public function postkomentar(Request $request){
        $request->request->add(['users_id' => auth()->user()->id]);
        $komentar = komentar::create($request->all());
        return redirect()->back()->with('sukses','Komentar berhasil ditambahkan');
    }

    public function destroy($id)
    {
        pertanyaan::where('id', $id)->delete();
        komentar::where('id', $id)->delete();
        komentar::where('parent', $id)->delete();
        return redirect('/forum')->with('eror', 'data anda berhasil di hapus');
    }
}
